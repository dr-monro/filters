#ifndef RESONANTFILTER_H
#define RESONANTFILTER_H

#include <vector>
#include <stdint.h>

class ResonantFilter
{
public:
    ResonantFilter(double fSamp, double fRes, double rob);
    std::vector<double> calcFilter(std::vector<double> inp);
    void setFreq(double freq);
private:
    double xn1;
    double xn2;
    double yn1;
    double yn2;
    double r;
    double fSamp;
    double w0;
};

#endif // RESONANTFILTER_H

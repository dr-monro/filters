#ifndef FILTRATION_H
#define FILTRATION_H
#include <vector>
#include "fifo.h"
class Filtration
{
public:
    Filtration(std::vector<double> vectorCoffsInp, std::vector<double> vectorCoffsOut);
    ~Filtration()
    {
        delete inFifo;
        delete outFifo;
    }

    double calcFilt(double newInp);
    static std::vector<double> decimate(std::vector<double> input, uint16_t d);
    static std::vector<double> movefreq(std::vector<double> input, double curRotFr, double curSampleRate);
private:
    Fifo *inFifo ;
    Fifo *outFifo ;
    std::vector<double> vectorCoffsInp ;
    std::vector<double> vectorCoffsOut ;
};

#endif // FILTRATION_H

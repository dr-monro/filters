#ifndef FIFO_H
#define FIFO_H
#include <stdint.h>
#include <vector>
class Fifo
{
public:
    Fifo(uint32_t sizeBuf);
    void addValue(double value);
    double getValue(uint32_t iterator);
    uint32_t retSizeBuf(){return sizeBuf;}
private:
    uint32_t head;
    uint32_t tail;
    uint32_t sizeBuf;
    std::vector<double> inVector;
    bool flagFilled;
};

#endif // FIFO_H

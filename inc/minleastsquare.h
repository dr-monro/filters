#ifndef MINLEASTSQUARE_H
#define MINLEASTSQUARE_H
#include <vector>
#include <stdint.h>
using namespace std;

class MlsCoffs
{
public:
    double g1;
    double g2;
    double g3;
    double g4;
    double g5;
    double g6;
    double g7;
    double g8;
    double g9;
    double As;
public:
    MlsCoffs():g1(0),g2(0),g3(0),g4(0),g5(0),g6(0),g7(0),g8(0),g9(0),As(0)
    {

    }
    void reset(){
        g1=0;
        g2=0;
        g3=0;
        g4=0;
        g5=0;
        g6=0;
        g7=0;
        g8=0;
        g9=0;
        As=0;
    }
};

class MlsOuts
{
public:

    double a;
    double b;


    MlsOuts():a(0),b(0){}
    static MlsOuts calcMlsOuts(MlsCoffs v)
    {
        MlsOuts out;
        double d, d2, d3;
        d = v.g9*v.g1*v.g2 + 2 * v.g7*v.g8*v.g3 - v.g8*v.g8*v.g1 - v.g7*v.g7*v.g2 - v.g9*v.g3*v.g3;
        d2 = v.g9*v.g4*v.g2 + v.g7*v.g8*v.g5 + v.g6*v.g3*v.g8 - v.g8*v.g8*v.g4 - v.g7*v.g6*v.g2 - v.g5*v.g3*v.g9;
        d3 = v.g9*v.g1*v.g5 + v.g7*v.g3*v.g6 + v.g7*v.g4*v.g8 - v.g8*v.g1*v.g6 - v.g7*v.g7*v.g5 - v.g3*v.g4*v.g9;
        out.a = d2 / d;
        out.b = d3 / d;
        return out;
    }

};

class MinLeastSquare
{

public:
    typedef struct{
        double magn;
        double phase;
    }OutMLS;
    MinLeastSquare();
    OutMLS calc(std::vector<double> vals);
    bool doMeasure(std::vector<int> vals, bool warming);
    MlsCoffs recalcCoffs(MlsCoffs cur, double x1, double x2, double val);
    void resetPlane(){plane.reset();}
private:
    MlsCoffs plane;


};

#endif // MINLEASTSQUARE_H

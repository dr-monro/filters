#ifndef FULLFILTER_H
#define FULLFILTER_H

#include "filtration.h"
#include <vector>

class FullFilter
{
public:
    FullFilter();
    ~FullFilter()
    {
        delete filt1;
        delete filt2;
        delete filt3;
    }

    std::vector<double> calcFullFilt(std::vector<double> inpVect, double curRotFreq, double curSampleRate);
private:
     Filtration *filt2;
     Filtration *filt1;
     Filtration *filt3;

};

#endif // FULLFILTER_H

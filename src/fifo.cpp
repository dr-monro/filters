#include "fifo.h"
#include <assert.h>

Fifo::Fifo(uint32_t sizeBuf):head(0),tail(0),flagFilled(0),inVector(sizeBuf)
{
    this->sizeBuf=sizeBuf;

}

void Fifo::addValue(double value)
{
    //GetValue the less the newest
    if (this->head >= this->sizeBuf)
    {
        this->head=0;
    }
    this->inVector[this->head]=value;
    this->head=this->head+1;
    if (this->flagFilled)
        if(this->tail >= this->sizeBuf)
            this->tail=0;
        else
            this->tail=this->tail+1;

}

double Fifo::getValue(uint32_t iterator)
{
    if(iterator>this->sizeBuf)
    {
        assert(1);
        //%disp('outOfBuunds');
        //%this.inVector(0);
    }
    int32_t it=this->head-iterator-1;
    if(it<0)
        it=this->sizeBuf+it;
    double val=this->inVector[it];
    return val;
}

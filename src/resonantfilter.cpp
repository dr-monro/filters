#include "resonantfilter.h"
#include <math.h>
#define M_PI 3.1415
ResonantFilter::ResonantFilter(double fSamp, double fRes,double rob):xn1(0),xn2(0),yn1(0),yn2(0),r(rob)
{
    this->fSamp=fSamp;
    this->w0=(/*fSamp/2*/+fRes)/fSamp;
}

std::vector<double> ResonantFilter::calcFilter(std::vector<double> inp)
{
    std::vector<double> outp(inp.size());
    double out=0;
    double b0=0;
    double b1=0;
    double b2=0;
    double a1=0;
    double a2=0;
    uint32_t sZ=inp.size();
    for (uint32_t i=0;i<sZ;i++)
    {
        a1=2*r*cos(2*M_PI*(w0));
        a2=r*r;
        b0=(1-r*r)/2;
        b1=0;
        b2=-b0;
        out=b0*inp[i]+b1*xn1+b2*xn2-a1*yn1-a2*yn2;
        xn2=xn1;
        xn1=inp[i];
        yn2=yn1;
        yn1=out;
        outp[i]=out;
    }
    return outp;
}
void ResonantFilter::setFreq(double freq)
{
    this->w0=(fSamp/2+freq)/fSamp;
}

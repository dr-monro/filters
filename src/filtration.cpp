#include "filtration.h"
#include "assert.h"
#include <cmath>
# define M_PI           3.14159265358979323846  /* pi */
Filtration::Filtration(std::vector<double> vectorCoffsInp, std::vector<double> vectorCoffsOut):
    vectorCoffsInp(vectorCoffsInp),
    vectorCoffsOut(vectorCoffsOut)
{
    uint16_t n=0;
    n=(uint16_t)vectorCoffsOut.size();
    this->outFifo = new Fifo(n);
    n= (uint16_t)vectorCoffsInp.size();
    this->inFifo= new Fifo(n);
    //p.vectorCoffsInp=vectorCoffsInp;
    //p.vectorCoffsOut=vectorCoffsOut;
}

double Filtration::calcFilt(double newInp)
{
    double sumInp=0;
    double sumOutp=0;
    double p=0;
    this->inFifo->addValue(newInp);
    uint32_t i=0;
    for (i=0;i<this->inFifo->retSizeBuf();i++)
        sumInp=sumInp+this->inFifo->getValue(i)*this->vectorCoffsInp[i];
    i=0;
    for (i=0;i<this->outFifo->retSizeBuf();i++)
        sumOutp=sumOutp+this->outFifo->getValue(i)*this->vectorCoffsOut[i];
    p=sumInp+sumOutp;
    if (outFifo->retSizeBuf()>0)this->outFifo->addValue(p);
    return p;
}

std::vector<double> Filtration::decimate(std::vector<double> input,uint16_t d)
{
    std::vector<double> out(input.size()/d);
    for (size_t i=0;i<out.size();i++)
        out[i]=input[i*d];

    return out;
}

std::vector<double> Filtration::movefreq(std::vector<double> input,double curRotFr,double curSampleRate)
{
    double t=1/(double)curSampleRate;
    std::vector<double> out(input.size());
    uint32_t i=0;
    for (i=1;i<input.size()+1;i++)
        out[i-1]=input[i-1]*sin(2*M_PI*(curRotFr-1)*(i)*t);
    return out;

}


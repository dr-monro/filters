#include "minleastsquare.h"
#define M_PI 3.1415
MinLeastSquare::MinLeastSquare()
{

}

MinLeastSquare::OutMLS MinLeastSquare::calc(std::vector<double> vals)
{
    uint32_t curMeasure=0;
    double  nMeasures=vals.size();
    double A = 0;
    double delta =0;
    double phase=0;
    for (curMeasure=0;curMeasure<vals.size();curMeasure++){
        double rad = 2 * M_PI * curMeasure / nMeasures;
        double x1 = sin(rad);
        double x2 = cos(rad);
        plane=recalcCoffs(plane,  x1, x2, vals[curMeasure]);
        MlsOuts out=MlsOuts::calcMlsOuts(plane);
        A = sqrt(out.a*out.a + out.b*out.b);
        delta = fabs(plane.As - A) / plane.As;
        plane.As=A;
        phase=atan2(out.b,out.a);
    }

    OutMLS out;
    out.magn=A;
    out.phase=phase;
    nMeasures++;
    return out;

}


bool MinLeastSquare::doMeasure(std::vector<int> vals, bool warming)
{
//    int nPlanes = getPlanesCount(true);
//    int nMeasures = vals.size() / nPlanes;
//    double mDelta = g_cfg[Config::idMeasureDelta]->getF();
//    bool isNewSumAlg = g_cfg[Config::idMeasureNewSumAlg]->getB();
//    for (int curMeasure = 0; curMeasure < nMeasures; curMeasure++) {
//        double rad = 2 * M_PI * curMeasure / nMeasures;
//        int delta_res = 0;
//        for (int p = 0; p < nPlanes; p++) {
//            auto v = &_mTempVals[p];
//            int val = vals[curMeasure * nPlanes + p];
//            val *= v->scale;
//            double x1 = sin(rad);
//            double x2 = cos(rad);
//            if (isNewSumAlg) {
//                v->g1 += x1*x1;
//                v->g2 += x2*x2;
//                v->g3 += x1*x2;
//                v->g4 += x1*val;
//                v->g5 += x2*val;
//                v->g6 += val;
//                v->g7 += x1;
//                v->g8 += x2;
//                v->g9 += 1;
//            } else {
//                v->g1 += x1*x1;
//                v->g2 += x2*x2;
//                v->g3 += x1*x2;
//                v->g4 += x1*val;
//                v->g5 += x2*val;
//            }

//            if (warming) {
//                continue;
//            }


//            if (isNewSumAlg) {
//                double d, d2, d3;
//                d = v->g9*v->g1*v->g2 + 2 * v->g7*v->g8*v->g3 - v->g8*v->g8*v->g1 - v->g7*v->g7*v->g2 - v->g9*v->g3*v->g3;
//                d2 = v->g9*v->g4*v->g2 + v->g7*v->g8*v->g5 + v->g6*v->g3*v->g8 - v->g8*v->g8*v->g4 - v->g7*v->g6*v->g2 - v->g5*v->g3*v->g9;
//                d3 = v->g9*v->g1*v->g5 + v->g7*v->g3*v->g6 + v->g7*v->g4*v->g8 - v->g8*v->g1*v->g6 - v->g7*v->g7*v->g5 - v->g3*v->g4*v->g9;
//                v->a = d2 / d;
//                v->b = d3 / d;
//            } else {
//                v->a = (1 / v->g1)*(v->g4 - ((v->g1*v->g5 - v->g3*v->g4) /
//                                             (v->g1*v->g2 - v->g3*v->g3)*v->g3));
//                v->b = (v->g1*v->g5 - v->g3*v->g4) / (v->g1*v->g2 - v->g3*v->g3);
//            }

//            double A = sqrt(v->a*v->a + v->b*v->b);
//            double delta = fabs(v->As - A) / v->As;
//            if (delta <= mDelta) {
//                delta_res++;
//            }
//            v->As = A;
//        }

//        if (delta_res >= nPlanes) {
//            for (int i = 0; i < nPlanes; i++) {
//                double A, lambda;
//                double phi = CompA_PHI(_mTempVals[i].a, _mTempVals[i].b, A, lambda);
//                addMeasureResult(A, phi, lambda, _mTempVals[i].scale, _mTempVals[i].idx);
//            }
//            return true;
//        }
//    }
//    return false;
    return 0;
}

MlsCoffs MinLeastSquare::recalcCoffs(MlsCoffs cur, double x1, double x2, double val)
{
    MlsCoffs out;
    out.g1=cur.g1 + x1*x1;
    out.g2=cur.g2 + x2*x2;
    out.g3=cur.g3 + x1*x2;
    out.g4=cur.g4 + x1*val;
    out.g5=cur.g5 + x2*val;
    out.g6=cur.g6 + val;
    out.g7=cur.g7 + x1;
    out.g8=cur.g8 + x2;
    out.g9=cur.g9 + 1;
    out.As=cur.As;
    return out;
}

static double CompA_PHI(double a, double b, double &A, double &lambda)
{
    A = sqrt(a*a + b*b);
    int sign = 1;
    if (b < 0) {
        sign = -1;
    } else if (b == 0) {
        sign = 0;
    }
    double phi = sign*acos(a / A);
    lambda = 180 * phi / M_PI;
    if (lambda >= 360) {
        lambda -= 360;
    } else if (lambda <0) {
        lambda += 360;
    }
    return phi;
}
